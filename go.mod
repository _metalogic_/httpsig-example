module bitbucket.org/_metalogic_/examples/httpsig

go 1.17

require (
	bitbucket.org/_metalogic_/glib v0.9.12
	bitbucket.org/_metalogic_/log v1.4.1
	github.com/go-fed/httpsig v1.1.0
)

require (
	bitbucket.org/_metalogic_/color v1.0.4 // indirect
	bitbucket.org/_metalogic_/colorable v1.0.3 // indirect
	bitbucket.org/_metalogic_/config v1.3.1 // indirect
	bitbucket.org/_metalogic_/isatty v1.0.4 // indirect
	golang.org/x/crypto v0.0.0-20200622213623-75b288015ac9 // indirect
	golang.org/x/sys v0.0.0-20210809222454-d867a43fc93e // indirect
)
