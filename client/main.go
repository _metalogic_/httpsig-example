package main

import (
	"bytes"
	"crypto/rsa"
	"crypto/sha256"
	"crypto/x509"
	"encoding/base64"
	"encoding/pem"
	"flag"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"bitbucket.org/_metalogic_/config"
	"bitbucket.org/_metalogic_/log"
	"github.com/go-fed/httpsig"
)

const (
	testSpecBody = `{"hello": "world"}`
	testSpecDate = `Sun, 05 Jan 2014 21:31:40 GMT`
)

var (
	keyFlg    string
	levelFlg  log.Level
	tenantFlg string
	tokenFlg  string
	urlFlg    string
)

func init() {
	flag.StringVar(&keyFlg, "key", "spuzzum", "name of RSA private key file")
	flag.StringVar(&tenantFlg, "id", "2D03D677-6D64-4F36-B098-9CA487E3B6EA", "tenant GUID")
	flag.StringVar(&tokenFlg, "token", "SPUZZUM_API_TOKEN", "name of bearer token")
	flag.StringVar(&urlFlg, "url", "http://localhost:8080/verify", "target URL")
	flag.Var(&levelFlg, "level", "set log level to one of trace, debug, info, warning, error")
}

// example call:
// $ client -url "https://dev-apis.educationplannerbc.ca/test-api/v1/tests/2D03D677-6D64-4F36-B098-9CA487E3B6EA"

func main() {

	flag.Parse()

	if levelFlg != log.None {
		log.SetLevel(levelFlg)
	} else {
		loglevel := os.Getenv("LOG_LEVEL")
		if loglevel == "DEBUG" {
			log.SetLevel(log.DebugLevel)
		}
	}

	token := config.MustGetConfig(tokenFlg)

	// sign request with RSA private key from file
	privateKeyPEM, err := ioutil.ReadFile(keyFlg + ".private")
	if err != nil {
		log.Fatalf("failed to read private key: %s", err)
	}

	privateKey, err := loadPrivateKey(privateKeyPEM)
	if err != nil {
		log.Fatalf("failed to load private key %s", err)
	}

	url := urlFlg + "/" + tenantFlg
	response, err := post(url, tenantFlg, token, privateKey)
	if err != nil {
		log.Error(err)
	}
	fmt.Println(response)

}

func post(url, keyID, token string, privKey *rsa.PrivateKey) (response string, err error) {
	var Signature httpsig.SignatureScheme = "Signature"
	r, err := http.NewRequest("POST", url, bytes.NewBuffer([]byte(testSpecBody)))
	if err != nil {
		return response, fmt.Errorf("error creating request: %s", err)
	}

	// headers := []string{"(request-target)", "host", "date", "content-type", "digest", "content-length"}
	// fails for "(request-target)",

	headers := []string{"digest", "host", "date", "content-type"}

	r.Header["Date"] = []string{testSpecDate}
	r.Header["Host"] = []string{r.URL.Host}
	r.Header["Content-Type"] = []string{"application/json"}

	r.Header["Authorization"] = []string{"Bearer " + token}

	// set the Digest header
	setDigest(r)

	s, _, err := httpsig.NewSigner([]httpsig.Algorithm{httpsig.RSA_SHA256}, httpsig.DigestSha256, headers, Signature, 0)
	if err != nil {
		return response, fmt.Errorf("error creating signer: %s", err)
	}

	if err := s.SignRequest(privKey, keyID, r, nil); err != nil {
		return response, fmt.Errorf("error signing request: %s", err)
	}

	client := &http.Client{}

	resp, err := client.Do(r)
	if err != nil {
		return response, err
	}

	bs, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return response, err
	}
	response = string(bs)

	return response, nil
}

func loadPrivateKey(keyData []byte) (*rsa.PrivateKey, error) {
	pem, _ := pem.Decode(keyData)
	if pem.Type != "RSA PRIVATE KEY" {
		return nil, fmt.Errorf("RSA private key is of the wrong type: %s", pem.Type)
	}

	return x509.ParsePKCS1PrivateKey(pem.Bytes)
}

func setDigest(r *http.Request) ([]byte, error) {
	var bodyBytes []byte
	if _, ok := r.Header["Digest"]; !ok {
		body := ""
		if r.Body != nil {
			var err error
			bodyBytes, err = ioutil.ReadAll(r.Body)
			if err != nil {
				return nil, fmt.Errorf("error reading body. %v", err)
			}

			// And now set a new body, which will simulate the same data we read:
			r.Body = ioutil.NopCloser(bytes.NewBuffer(bodyBytes))
			body = string(bodyBytes)
		}

		d := sha256.Sum256([]byte(body))
		r.Header["Digest"] = []string{fmt.Sprintf("SHA-256=%s", base64.StdEncoding.EncodeToString(d[:]))}
	}

	return bodyBytes, nil
}
