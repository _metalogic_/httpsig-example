#!/bin/bash

if [ $# != 1 ]; then
   echo usage: $0 key-name
   exit 1
fi

name=$1

openssl genrsa -out $name.private 4096

openssl rsa -in $name.private -out $name.public -pubout -outform PEM
