package main

import (
	"crypto/rsa"
	"crypto/x509"
	"encoding/pem"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"

	. "bitbucket.org/_metalogic_/glib/http"
	"bitbucket.org/_metalogic_/log"
	"github.com/go-fed/httpsig"
)

const (
	verifyJSON = `{ "status": "signature is verified" }`

	// the Spuzzum tenantID
	spuzzumID = "2D03D677-6D64-4F36-B098-9CA487E3B6EA"
)

// pubKeys is a map of keyIDs to their respective public keys
var pubKeys map[string][]byte

func init() {
	spuzzumPublicKeyPEM, err := ioutil.ReadFile("spuzzum.public")
	if err != nil {
		log.Fatalf("failed to read public key from %s: %s", "spuzzum.public", err)
	}

	pubKeys = make(map[string][]byte)
	pubKeys[spuzzumID] = spuzzumPublicKeyPEM
}

func main() {

	log.Debug("starting server")

	http.HandleFunc("/verify", verifyHandler)

	http.ListenAndServe(":8080", nil)
}

//  The pubKeyId will be used at verification time.
func verifyHandler(w http.ResponseWriter, r *http.Request) {
	for name, headers := range r.Header {
		for _, h := range headers {
			fmt.Fprintf(w, "%v: %v\n------------------\n", name, h)
		}
	}

	var mu sync.Mutex
	mu.Lock()
	defer mu.Unlock()

	err := verify(r)
	if err != nil {
		ErrJSON(w, err)
		return
	}
	OkJSON(w, verifyJSON)
}

// Verifying requires an application to use the pubKeyId to both retrieve the key needed for verification
// as well as determine the algorithm to use. Use a Verifier:
func verify(r *http.Request) error {
	verifier, err := httpsig.NewVerifier(r)
	if err != nil {
		return err
	}

	pubKeyID := verifier.KeyId()
	pubKey, found := pubKeys[pubKeyID]
	if !found {
		return fmt.Errorf("public key not found for %s", pubKeyID)
	}

	rsa, err := loadPublicKey(pubKey)
	if err != nil {
		return err
	}

	// The verifier will verify the Digest in addition to the HTTP signature
	return verifier.Verify(rsa, httpsig.RSA_SHA256)
}

func loadPublicKey(keyData []byte) (*rsa.PublicKey, error) {
	pem, _ := pem.Decode(keyData)
	if pem == nil {
		return nil, fmt.Errorf("failed to decode public key %s", string(keyData))
	}
	if pem.Type != "PUBLIC KEY" {
		return nil, fmt.Errorf("public key is of the wrong type: %s", pem.Type)
	}

	key, err := x509.ParsePKIXPublicKey(pem.Bytes)
	if err != nil {
		return nil, err
	}

	return key.(*rsa.PublicKey), nil
}
